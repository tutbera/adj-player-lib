import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  playerData = {
    play: true,
    fitscreen: true,
    fullscreen: true,
    volume: true
  };
  title = 'app';
}
