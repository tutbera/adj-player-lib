import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PlayerLibModule } from 'player-lib';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    PlayerLibModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
