export function formateTime(number: number): string {
  let result = '',
    minutes: number,
    seconds: number;
  minutes = Math.floor(number / 60);
  seconds = Math.floor(number % 60);

  result += (minutes >= 10) ? minutes : '0' + minutes;
  result += ':';
  result += (seconds >= 10) ? seconds : '0' + seconds;

  return result;
}

