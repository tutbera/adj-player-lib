import {Component, Inject, OnInit, PLATFORM_ID} from '@angular/core';
import {FullScreen} from '../../core/states/Fullscreen';
import {isPlatformBrowser} from '@angular/common';

@Component({
  selector: '.adj-fullscreen',
  template: `
    <button class="f-shrink-0 text-color-secondary background-transparent border-full-0 order-6"
            (click)="fullScreen.toggle()">
      <span class="icon icon-xl f-plico icon-fullscreen"></span>
    </button>
  `
})
export class FullscreenComponent implements OnInit {


  constructor(public fullScreen: FullScreen, @Inject(PLATFORM_ID) private platformId: Object) {
  }

  ngOnInit() {
    if (!isPlatformBrowser(this.platformId)) return;

    const fullscreen = document.body.querySelector('#fullscreen');

    this.fullScreen.setElement(fullscreen);

    fullscreen.addEventListener('keydown', (event) => {
      event['keyCode'] === 70 && this.fullScreen.toggle();
    });

  }
}
