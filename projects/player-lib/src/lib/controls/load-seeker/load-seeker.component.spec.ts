import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadSeekerComponent } from './load-seeker.component';

describe('LoadSeekerComponent', () => {
  let component: LoadSeekerComponent;
  let fixture: ComponentFixture<LoadSeekerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadSeekerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadSeekerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
