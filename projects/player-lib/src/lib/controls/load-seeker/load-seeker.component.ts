import {Component, Inject, OnInit, PLATFORM_ID, ViewChild} from '@angular/core';
import {isPlatformBrowser} from '@angular/common';

@Component({
  selector: 'app-load-seeker',
  template: `
    <div #loader class="y-100 pos-absolute pos-left-0 pos-top-0 pos-bottom-0 m-auto index-5 background-secondary"></div>
  `,
  styles: []
})
export class LoadSeekerComponent implements OnInit {

  @ViewChild('loader') loader;

  constructor(@Inject(PLATFORM_ID) private platformId: Object) { }

  ngOnInit() {
    if (!isPlatformBrowser(this.platformId)) return;
    const player = document.body.querySelector('#videoPlayer');
    player.addEventListener('progress', (event) => {
      this.Load(event , this.loader.nativeElement );
    });

  }

  Load(event, f) {
    if (event.srcElement.buffered.length > 0) {
      const procent = (event.srcElement.buffered.end(event.srcElement.buffered.length - 1) / event.srcElement.duration) * 100;
      f.style.width = procent + '%';
    }
  }

}
