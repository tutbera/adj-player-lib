import {Component, Inject, OnInit, PLATFORM_ID} from '@angular/core';
import {VolumeMute} from '../../core/states/Volume-Mute';
import {isPlatformBrowser} from '@angular/common';

@Component({
  selector: '.adj-volume-mute',
  template: `
    <div class="mr-20 order-3 pos-relative volume" (click)="volume.toggle('mute')">
      <button class="f-shrink-0 text-color-secondary background-transparent border-full-0">
        <span class="icon icon-xl f-plico icon-sound"></span>
      </button>
      <!--sound indicator-->
      <div class="pos-absolute pos-left-0 x-2 y-90 pos-right-0 m-auto pos-top-0 p-10 box-content transform-y-100 sound-indicator">
        <adj-seeker [max]="1" *ngIf="player" [(model)]="player.volume" (modelChange)="volume.toggle('volume', $event)" [modifier]="['seeker-y']"
                [type]="1"></adj-seeker>
      </div>
      <!--sound indicator-->
    </div>`
})
export class VolumeMuteComponent implements OnInit {

  player;

  constructor(public volume: VolumeMute, @Inject(PLATFORM_ID) private platformId: Object) {
  }

  ngOnInit() {
    if (!isPlatformBrowser(this.platformId)) return;
    this.player = document.body.querySelector('#videoPlayer');
    this.volume.setElement(this.player);

    const fullscreen = document.body.querySelector('#fullscreen');
    fullscreen.addEventListener('keydown', (event) => {
      event['keyCode'] === 77 &&  this.volume.toggle('mute');
      event['keyCode'] === 38 &&  this.volume.toggle('volume', this.player.volume + 0.1);
      event['keyCode'] === 40 &&  this.volume.toggle('volume', this.player.volume - 0.1);
    });

  }

}
