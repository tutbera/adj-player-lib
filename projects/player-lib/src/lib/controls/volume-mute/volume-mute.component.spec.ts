import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VolumeMuteComponent } from './volume-mute.component';

describe('VolumeMuteComponent', () => {
  let component: VolumeMuteComponent;
  let fixture: ComponentFixture<VolumeMuteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VolumeMuteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VolumeMuteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
