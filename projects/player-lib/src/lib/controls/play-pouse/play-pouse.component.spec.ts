import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayPouseComponent } from './play-pouse.component';

describe('PlayPouseComponent', () => {
  let component: PlayPouseComponent;
  let fixture: ComponentFixture<PlayPouseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayPouseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayPouseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
