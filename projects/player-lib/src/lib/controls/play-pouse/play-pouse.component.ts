import {Component, Inject, Input, OnInit, PLATFORM_ID} from '@angular/core';
import {PlayPause} from '../../core/states/Play-Pause';
import {isPlatformBrowser} from '@angular/common';

@Component({
  selector: '.adj-play-pouse',
  template: `
    <button class="background-transparent border-full-0  text-color-secondary mr-20" (click)="play.toggle()"
            [class.active]="play.isEnabled()" >
      <span class="icon f-plico icon-player icon-xl"></span>
    </button>
  `
})
export class PlayPouseComponent implements OnInit {

  player;

  constructor(public play: PlayPause, @Inject(PLATFORM_ID) private platformId: Object) {
  }

  ngOnInit() {
    if (!isPlatformBrowser(this.platformId)) return;

    this.player = document.body.querySelector('#videoPlayer');
    this.play.setElement(this.player);
    const fullscreen = document.body.querySelector('#fullscreen');
    fullscreen.addEventListener('keydown', (event) => {
      event['keyCode'] === 32 && this.play.toggle();
    });
  }

}
