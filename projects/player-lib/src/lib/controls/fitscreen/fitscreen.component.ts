import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'adj-fitscreen',
  template: `
    <!--fit screen-->
    <button class="f-shrink-0 text-color-secondary background-transparent border-full-0 mr-20 order-6">
      <span class="icon icon-xl f-plico icon-fit-land"></span>
    </button>
    <!--fit screen-->
  `
})
export class FitscreenComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
