import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FitscreenComponent } from './fitscreen.component';

describe('FitscreenComponent', () => {
  let component: FitscreenComponent;
  let fixture: ComponentFixture<FitscreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FitscreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FitscreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
