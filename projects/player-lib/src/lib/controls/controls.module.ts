import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FullscreenComponent } from './fullscreen/fullscreen.component';
import { VolumeMuteComponent } from './volume-mute/volume-mute.component';
import {SharedModule} from '../core/shared-components/shared.module';
import { PlayPouseComponent } from './play-pouse/play-pouse.component';
import { FitscreenComponent } from './fitscreen/fitscreen.component';
import { LoadSeekerComponent } from './load-seeker/load-seeker.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [
    FullscreenComponent,
    VolumeMuteComponent,
    PlayPouseComponent,
    FitscreenComponent,
    LoadSeekerComponent
  ],
  entryComponents: [
    FullscreenComponent,
    VolumeMuteComponent,
    PlayPouseComponent,
    FitscreenComponent,
    LoadSeekerComponent
  ],
  exports: [
    FullscreenComponent,
    VolumeMuteComponent,
    PlayPouseComponent,
    FitscreenComponent,
    LoadSeekerComponent
  ],
  providers: [
  ]
})
export class ControlsModule { }
