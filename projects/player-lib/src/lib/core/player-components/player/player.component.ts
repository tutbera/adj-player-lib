import {AfterContentInit, Component, ComponentFactoryResolver, ElementRef, Inject, Input, OnInit, ViewChild, ViewContainerRef, ViewEncapsulation} from '@angular/core';
import {isPlatformBrowser, isPlatformServer} from '@angular/common';
import {formateTime } from '../../../helper/player.helper';
import {FullscreenComponent} from '../../../controls/fullscreen/fullscreen.component';
import {VolumeMuteComponent} from '../../../controls/volume-mute/volume-mute.component';
import {PlayPouseComponent} from '../../../controls/play-pouse/play-pouse.component';
import {FitscreenComponent} from '../../../controls/fitscreen/fitscreen.component';

@Component({
  selector: 'adj-player',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss'],
  // host: {
  //   '(document:click)': 'onClick($event)',
  // }
})
export class PlayerComponent implements OnInit, AfterContentInit  {

  @ViewChild('Player') public player: ElementRef;
  @ViewChild('cont' , {read: ViewContainerRef}) cont ;
  @Input('config') config = {
    play: true,
    fullscreen: true,
    fitscreen: false,
    volume: true
  };

  public videoSrc = 'http://s1265-l01.imovies.cc/video/imovie_hash_code/7/2018010411222279_high_eng.mp4?md5=xTXNQONy2fubqfhumKi72w&expires=1538046568';

  // currenttime / duration
  public CurrentTime = '00:00' ;
  public Duration = '00:00';
  // currenttime / duration end

  constructor( private  element: ElementRef, private resolver: ComponentFactoryResolver) {
  }

  ngOnInit() {}

  ngAfterContentInit() {
    if (this.config.play) {
      const playFactory = this.resolver.resolveComponentFactory( PlayPouseComponent );
      this.cont.createComponent(playFactory)._viewRef.rootNodes[0].classList.add('order-0');
    }
    if (this.config.volume) {
      const volume = this.resolver.resolveComponentFactory( VolumeMuteComponent );
      this.cont.createComponent(volume)._viewRef.rootNodes[0].classList.add('order-3');
    }
    if (this.config.fullscreen) {
      const fullscreen = this.resolver.resolveComponentFactory( FullscreenComponent );
      this.cont.createComponent(fullscreen)._viewRef.rootNodes[0].classList.add('order-7');
    }
    if (this.config.fitscreen) {
      const fitscreen = this.resolver.resolveComponentFactory( FitscreenComponent );
      this.cont.createComponent(fitscreen)._viewRef.rootNodes[0].classList.add('order-6');
    }
  }

  timeUpdate() {
    // this.CurrentTime = formateTime(this.player.nativeElement.currentTime);
    // this.Duration = formateTime(this.player.nativeElement.duration);
  }

  PlayOn(value: number) {
    // this.player.nativeElement.currentTime = value;
    // this.player.nativeElement.play();
  }


  onKeyDown(event: any) {
    switch (event.keyCode) {
      case 37:
        // this.player.nativeElement.currentTime -= 2;
        break;
      case 39:
        // this.player.nativeElement.currentTime += 2;
        break;
      default:
        break;
    }
  }

}
