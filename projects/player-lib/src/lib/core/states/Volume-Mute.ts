import {Inject, Injectable, PLATFORM_ID} from '@angular/core';
import {isPlatformBrowser} from '@angular/common';

@Injectable()

export class VolumeMute {

  private element: any;

  constructor(@Inject(PLATFORM_ID) private platformId: Object) {

    if (!isPlatformBrowser(this.platformId)) return;
    this.element = Element;
  }

  setElement(element: Element) {
    if (!isPlatformBrowser(this.platformId)) return;
    this.element = element;
  }

  isEnabled() {
    return this.element['volume'] === 0;
  }

  toggle(type: string, value?: number) {
    if (!isPlatformBrowser(this.platformId)) return;
    switch (type) {
      case 'mute':
        this.isEnabled() ? this.unmute() : this.mute();
        break;
      case 'volume':
        if (value > 0 && value <= 1) {
          this.volume(value);
        }
        break;

    }
  }

  volume(value: number) {
    this.element['volume'] = value;
  }

  mute() {
    this.element['volume'] = 0;

  }

  unmute() {
    this.element['volume'] = 1;

  }
}

