import {Inject, Injectable, PLATFORM_ID} from '@angular/core';
import {isPlatformBrowser} from '@angular/common';

@Injectable()
export class FullScreen {

  private document: any;

  private fullScreenPropertyName: string;

  private requestFullScreenMethodName: string;

  private exitFullScreenMethodName: string;

  private element: Element;

  constructor(@Inject(PLATFORM_ID) private platformId: Object) {
    if (!isPlatformBrowser(this.platformId)) return;
    this.document = document;
    this.fullScreenPropertyName = [
      'fullScreen',
      'mozFullScreen',
      'webkitIsFullScreen'
    ].find((property: string) => {
      return typeof this.document[property] !== 'undefined';
    });

    this.requestFullScreenMethodName = [
      'requestFullScreen',
      'mozRequestFullScreen',
      'webkitEnterFullscreen',
      'webkitRequestFullscreen',
      'msRequestFullscreen',
      'webkitExitFullscreen'
    ].find((property: string) => {
      return typeof this.document.documentElement[property] !== 'undefined';
    });

    this.exitFullScreenMethodName = [
      'mozExitFullScreen',
      'exitFullscreen',
      'cancelFullscreen',
      'mozCancelFullScreen',
      'msExitFullscreen',
      'webkitExitFullscreen'
    ].find((property: string) => {
      return typeof this.document[property] !== 'undefined';
    });
    this.element = this.document.documentElement;

  }

  setElement(element: Element) {
    this.element = element;
  }

  isSupported() {
    return !!this.fullScreenPropertyName;
  }

  isEnabled() {
    if (!isPlatformBrowser(this.platformId)) return;
    if (this.isSupported()) {
      return this.document[this.fullScreenPropertyName];
    }
    return false;
  }

  enable() {
    if (!isPlatformBrowser(this.platformId)) return;
    if (this.isSupported()) {
      this.element[this.requestFullScreenMethodName]();
    }
  }

  disable() {

    if (!isPlatformBrowser(this.platformId)) return;
    if (this.isSupported()) {
      this.document[this.exitFullScreenMethodName]();
    }

  }

  toggle() {
    if (this.isEnabled()) {
      this.disable();
    } else {
      this.enable();
    }
  }
}
