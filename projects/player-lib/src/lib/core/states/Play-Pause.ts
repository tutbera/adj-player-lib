import {Inject, Injectable, PLATFORM_ID} from '@angular/core';
import {isPlatformBrowser} from '@angular/common';

@Injectable()
export class PlayPause {

  private element: any;

  constructor(@Inject(PLATFORM_ID) private platformId: Object) {
  }

  setElement(element: Element) {
    if (!isPlatformBrowser(this.platformId)) return;
      this.element = element;
  }

  isEnabled() {
    if (!isPlatformBrowser(this.platformId)) return;
    this.status()
    return this.element['playing'];
  }

  status() {
    Object.defineProperty(HTMLMediaElement.prototype, 'playing', {
      get: function () {
        return !!(this.currentTime > 0 && !this.paused && !this.ended && this.readyState > 2);
      }
    });

  }

  enable() {
    this.element.play();
  }

  disable() {
    this.element.pause();
  }

  toggle() {
    if (this.isEnabled()) {
      this.disable();
    } else {
      this.enable();
    }
  }
}
