import {
  Component,
  ComponentFactoryResolver,
  ElementRef,
  EventEmitter,
  Inject,
  Input,
  OnInit,
  Output,
  Renderer2,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {PLATFORM_ID} from '@angular/core';
import {isPlatformBrowser, isPlatformServer} from '@angular/common';
import {LoadSeekerComponent} from '../../../controls/load-seeker/load-seeker.component';

@Component({
  selector: 'adj-seeker',
  templateUrl: './seeker.component.html',
  styleUrls: ['./seeker.component.scss']
})
export class SeekerComponent implements OnInit {

  @Input() max = 100;
  @Input() model: number;
  @Input() public modifier: string[];
  @Output() modelChange: EventEmitter<number> = new EventEmitter();

  public classNames: String;

  @ViewChild('cont', {read: ViewContainerRef}) cont;

  @Input() public type: number;
  private workSpace = [
    {
      dimension: 'clientX',
      from: 'left',
      value: 'width'
    },
    {
      dimension: 'clientY',
      from: 'top',
      value: 'height'
    }
  ];

  private rect: any;
  private destroyMouseMove: any;
  private destroyMouseUp: any;

  constructor(@Inject(PLATFORM_ID) private platformId: Object, @Inject(ElementRef) public element: ElementRef,
              @Inject(Renderer2) public renderer: Renderer2, private resolver: ComponentFactoryResolver) {
  }

  ngOnInit(): any {
    this.classNames = !this.modifier ? '' : [''].concat(this.modifier.map((modifier) => modifier)).join(' ');
    if (this.type === 0) {
      const loadscreen = this.resolver.resolveComponentFactory(LoadSeekerComponent);
      this.cont.createComponent(loadscreen)._viewRef.rootNodes[0].classList.add('order-6');
    }
    if (isPlatformBrowser(this.platformId)) {
      this.renderer.listen(this.element.nativeElement, 'mousedown', (event: any) => this.begin(event));
    }
  }


  begin(event: any) {
    if (isPlatformBrowser(this.platformId)) {
      const process = 'mousemove', end = 'mouseup';
      this.rect = this.element.nativeElement.getBoundingClientRect();
      this.end();
      this.destroyMouseMove = this.renderer.listen('document', process, (e: any) => this.process(e));
      this.destroyMouseUp = this.renderer.listen('document', end, () => this.end());
      this.process(event);
    }
  }

  end() {
    if (this.destroyMouseMove) {
      this.destroyMouseMove();
      this.destroyMouseMove = null;
    }
    if (this.destroyMouseUp) {
      this.destroyMouseUp();
      this.destroyMouseUp = null;
    }
  }

  process(e: any) {
    let value: number;
    value = e[this.workSpace[this.type].dimension] - this.rect[this.workSpace[this.type].from];
    value = Math.max(value, 0);
    value = Math.min(value, this.rect[this.workSpace[this.type].value]);
    value = value / this.rect[this.workSpace[this.type].value];
    value = this.max * value;
    if (this.type === 1) value = 1 - value;
    this.modelChange.emit(value);
  }


}
