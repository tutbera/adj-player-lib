import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SeekerComponent} from './seeker/seeker.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    SeekerComponent
  ],
  exports: [
    SeekerComponent
  ],
})
export class SharedModule {}
