///<reference path="core/player-components/player/player.component.ts"/>
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayerComponent } from './core/player-components/player/player.component';
import {VolumeMute} from './core/states/Volume-Mute';
import {PlayPause} from './core/states/Play-Pause';
import {ControlsModule} from './controls/controls.module';
import {SharedModule} from './core/shared-components/shared.module';
import { PlayerLibComponent } from './player-lib.component';
import {FullScreen} from './core/states/Fullscreen';

@NgModule({
  imports: [
    CommonModule,
    ControlsModule,
    SharedModule
  ],
  declarations: [
    PlayerLibComponent,
    PlayerComponent
  ],
  exports: [
    PlayerComponent,
  ],
  providers: [
    FullScreen,
    VolumeMute,
    PlayPause
  ]
})
export class PlayerLibModule { }
