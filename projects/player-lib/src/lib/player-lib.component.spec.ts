import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerLibComponent } from './player-lib.component';

describe('PlayerLibComponent', () => {
  let component: PlayerLibComponent;
  let fixture: ComponentFixture<PlayerLibComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerLibComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
