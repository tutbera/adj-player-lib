import { TestBed, inject } from '@angular/core/testing';

import { PlayerLibService } from './player-lib.service';

describe('PlayerLibService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PlayerLibService]
    });
  });

  it('should be created', inject([PlayerLibService], (service: PlayerLibService) => {
    expect(service).toBeTruthy();
  }));
});
